package run

import (
	"context"
	"fmt"
	"net/http"
	"net/rpc/jsonrpc"
	"os"

	apb "gitlab.com/kuzmem/gateway/pkg/proto/auth_grpc"

	aservice "gitlab.com/kuzmem/gateway/internal/modules/auth/service"
	eservice "gitlab.com/kuzmem/gateway/internal/modules/exchange/service"
	uservice "gitlab.com/kuzmem/gateway/internal/modules/user/service"

	"google.golang.org/grpc/credentials/insecure"

	epb "gitlab.com/kuzmem/gateway/pkg/proto/exchange_grpc"
	upb "gitlab.com/kuzmem/gateway/pkg/proto/user_grpc"
	"google.golang.org/grpc"

	"github.com/go-chi/chi/v5"
	jsoniter "github.com/json-iterator/go"
	"github.com/ptflp/godecoder"
	"gitlab.com/kuzmem/gateway/config"
	"gitlab.com/kuzmem/gateway/internal/infrastructure/component"
	"gitlab.com/kuzmem/gateway/internal/infrastructure/errors"
	"gitlab.com/kuzmem/gateway/internal/infrastructure/responder"
	"gitlab.com/kuzmem/gateway/internal/infrastructure/router"
	"gitlab.com/kuzmem/gateway/internal/infrastructure/server"
	internal "gitlab.com/kuzmem/gateway/internal/infrastructure/service"
	"gitlab.com/kuzmem/gateway/internal/infrastructure/tools/cryptography"
	"gitlab.com/kuzmem/gateway/internal/modules"
	"gitlab.com/kuzmem/gateway/internal/provider"
	"go.uber.org/zap"
	"golang.org/x/sync/errgroup"
)

// Application - интерфейс приложения
type Application interface {
	Runner
	Bootstraper
}

// Runner - интерфейс запуска приложения
type Runner interface {
	Run() int
}

// Bootstraper - интерфейс инициализации приложения
type Bootstraper interface {
	Bootstrap(options ...interface{}) Runner
}

// App - структура приложения
type App struct {
	conf           config.AppConf
	logger         *zap.Logger
	srv            server.Server
	jsonRPC        server.Server
	rabbitNotifier server.Server
	Sig            chan os.Signal
	Servises       *modules.Services
}

// NewApp - конструктор приложения
func NewApp(conf config.AppConf, logger *zap.Logger) *App {
	return &App{conf: conf, logger: logger, Sig: make(chan os.Signal, 1)}
}

// Run - запуск приложения
func (a *App) Run() int {
	// на русском
	// создаем контекст для graceful shutdown
	ctx, cancel := context.WithCancel(context.Background())

	errGroup, ctx := errgroup.WithContext(ctx)

	// запускаем горутину для graceful shutdown
	// при получении сигнала SIGINT
	// вызываем cancel для контекста
	errGroup.Go(func() error {
		sigInt := <-a.Sig
		a.logger.Info("signal interrupt recieved", zap.Stringer("os_signal", sigInt))
		cancel()
		return nil
	})

	// запускаем http сервер
	errGroup.Go(func() error {
		err := a.srv.Serve(ctx)
		if err != nil && err != http.ErrServerClosed {
			a.logger.Error("app: server error", zap.Error(err))
			return err
		}
		return nil
	})

	errGroup.Go(func() error {
		err := a.rabbitNotifier.Serve(ctx)
		if err != nil {
			a.logger.Error("notifier: init error", zap.Error(err))
			return err
		}
		return nil
	})

	if err := errGroup.Wait(); err != nil {
		return errors.GeneralError
	}

	return errors.NoError
}

// Bootstrap - инициализация приложения
func (a *App) Bootstrap(options ...interface{}) Runner {
	// на русском
	// инициализация емейл провайдера
	email := provider.NewEmail(a.conf.Provider.Email, a.logger)
	// инициализация сервиса нотификации
	notifyEmail := internal.NewNotify(a.conf.Provider.Email, email, a.logger)
	rabbitNotify := internal.NewRabbitReceiver(a.logger)
	a.rabbitNotifier = rabbitNotify
	// инициализация менеджера токенов
	tokenManager := cryptography.NewTokenJWT(a.conf.Token)
	// инициализация декодера
	decoder := godecoder.NewDecoder(jsoniter.Config{
		EscapeHTML:             true,
		SortMapKeys:            true,
		ValidateJsonRawMessage: true,
		DisallowUnknownFields:  true,
	})
	// инициализация менеджера ответов сервера
	responseManager := responder.NewResponder(decoder, a.logger)
	// инициализация генератора uuid
	uuID := cryptography.NewUUIDGenerator()
	// инициализация хешера
	hash := cryptography.NewHash(uuID)
	// инициализация компонентов
	components := component.NewComponents(a.conf, notifyEmail, tokenManager, responseManager, decoder, hash, a.logger)

	// инициализация сервисов
	var (
		user uservice.Userer
		auth aservice.Auther
		exch eservice.Exchanger
	)
	if a.conf.UserRPC.Type == "JsonRPC" {
		// инициализация клиента для взаимодействия с сервисом пользователей
		clientUser, err := jsonrpc.Dial("tcp", fmt.Sprintf("%s:%s", a.conf.UserRPC.Host, a.conf.UserRPC.Port))
		if err != nil {
			a.logger.Fatal("error init user rpc client", zap.Error(err))
		}
		a.logger.Info("rpc user client connected")

		user = uservice.NewUserServiceJSONRPC(clientUser)
	} else if a.conf.UserRPC.Type == "GRPC" {
		conn, err := grpc.Dial(fmt.Sprintf(
			"%s:%s", a.conf.UserRPC.Host, a.conf.UserRPC.Port),
			grpc.WithTransportCredentials(insecure.NewCredentials()))
		if err != nil {
			a.logger.Fatal("error init user grpc client", zap.Error(err))
		}
		a.logger.Info("grpc user client connected")

		clientUser := upb.NewUsererClient(conn)

		user = uservice.NewUserServiceGRPC(clientUser)
	}

	if a.conf.AuthRPC.Type == "JsonRPC" {
		// инициализация клиента для взаимодействия с сервисом аутентификации
		clientAuth, err := jsonrpc.Dial("tcp", fmt.Sprintf("%s:%s", a.conf.AuthRPC.Host, a.conf.AuthRPC.Port))
		if err != nil {
			a.logger.Fatal("error init auth rpc client", zap.Error(err))
		}
		a.logger.Info("rpc auth client connected")

		auth = aservice.NewAuthServiceJSONRPC(clientAuth)
	} else if a.conf.AuthRPC.Type == "GRPC" {
		conn, err := grpc.Dial(fmt.Sprintf(
			"%s:%s", a.conf.AuthRPC.Host, a.conf.AuthRPC.Port),
			grpc.WithTransportCredentials(insecure.NewCredentials()))
		if err != nil {
			a.logger.Fatal("error init auth grpc client", zap.Error(err))
		}
		a.logger.Info("grpc auth client connected")

		clientAuth := apb.NewAutherClient(conn)

		auth = aservice.NewAuthServiceGRPC(clientAuth)
	}

	if a.conf.ExchRPC.Type == "JsonRPC" {
		// инициализация клиента для взаимодействия с сервисом криптовалюты
		clientExch, err := jsonrpc.Dial("tcp", fmt.Sprintf("%s:%s", a.conf.ExchRPC.Host, a.conf.ExchRPC.Port))
		if err != nil {
			a.logger.Fatal("error init auth rpc client", zap.Error(err))
		}
		a.logger.Info("rpc auth client connected")

		exch = eservice.NewExchangeServiceJSONRPC(clientExch)
	} else if a.conf.ExchRPC.Type == "GRPC" {
		conn, err := grpc.Dial(fmt.Sprintf(
			"%s:%s", a.conf.ExchRPC.Host, a.conf.ExchRPC.Port),
			grpc.WithTransportCredentials(insecure.NewCredentials()))
		if err != nil {
			a.logger.Fatal("error init auth grpc client", zap.Error(err))
		}
		a.logger.Info("grpc auth client connected")

		clientExch := epb.NewExchangerClient(conn)

		exch = eservice.NewExchangeServiceGRPC(clientExch, components.Logger)
	}

	services := modules.NewServices(user, auth, exch)
	a.Servises = services

	controllers := modules.NewControllers(services, components)
	// инициализация роутера
	var r *chi.Mux
	r = router.NewRouter(controllers, components)
	// конфигурация сервера
	srv := &http.Server{
		Addr:    fmt.Sprintf(":%s", a.conf.Server.Port),
		Handler: r,
	}
	// инициализация сервера
	a.srv = server.NewHttpServer(a.conf.Server, srv, a.logger)
	// возвращаем приложение
	return a
}
