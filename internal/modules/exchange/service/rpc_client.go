package service

import (
	"context"
	"net/rpc"

	"gitlab.com/kuzmem/gateway/internal/infrastructure/errors"
	"gitlab.com/kuzmem/gateway/pkg/processing/exchange"
)

type ExchangeServiceJSONRPC struct {
	client *rpc.Client
}

func NewExchangeServiceJSONRPC(client *rpc.Client) *ExchangeServiceJSONRPC {
	return &ExchangeServiceJSONRPC{client: client}
}

func (e *ExchangeServiceJSONRPC) GetPriceListMax(ctx context.Context) exchange.CryptoOut {
	var out exchange.CryptoOut
	err := e.client.Call("ExchangeServiceJSONRPC.GetPriceListMax", exchange.CryptoIn{}, &out)
	if err != nil {
		out.ErrorCode = errors.GetPriceListMaxErr
	}

	return out
}

func (e *ExchangeServiceJSONRPC) GetPriceListMin(ctx context.Context) exchange.CryptoOut {
	var out exchange.CryptoOut
	err := e.client.Call("ExchangeServiceJSONRPC.GetPriceListMin", exchange.CryptoIn{}, &out)
	if err != nil {
		out.ErrorCode = errors.GetPriceListMinErr
	}

	return out
}

func (e *ExchangeServiceJSONRPC) GetPriceListAvg(ctx context.Context) exchange.CryptoOut {
	var out exchange.CryptoOut
	err := e.client.Call("ExchangeServiceJSONRPC.GetPriceListAvg", exchange.CryptoIn{}, &out)
	if err != nil {
		out.ErrorCode = errors.GetPriceListAvgErr
	}

	return out
}
