package service

import (
	"context"

	"gitlab.com/kuzmem/gateway/pkg/processing/exchange"
)

type Exchanger interface {
	GetPriceListMax(ctx context.Context) exchange.CryptoOut
	GetPriceListMin(ctx context.Context) exchange.CryptoOut
	GetPriceListAvg(ctx context.Context) exchange.CryptoOut
}
