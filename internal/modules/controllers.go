package modules

import (
	"gitlab.com/kuzmem/gateway/internal/infrastructure/component"
	aucontroller "gitlab.com/kuzmem/gateway/internal/modules/auth/controller"
	econtroller "gitlab.com/kuzmem/gateway/internal/modules/exchange/controller"
	ucontroller "gitlab.com/kuzmem/gateway/internal/modules/user/controller"
)

type Controllers struct {
	User     ucontroller.Userer
	Auth     aucontroller.Auther
	Exchange econtroller.Exchanger
}

func NewControllers(services *Services, components *component.Components) *Controllers {
	userController := ucontroller.NewUser(services.User, components)
	authController := aucontroller.NewAuth(services.Auth, components)
	exchangeController := econtroller.NewExchange(services.Exchange, components)
	return &Controllers{
		User:     userController,
		Auth:     authController,
		Exchange: exchangeController,
	}
}
