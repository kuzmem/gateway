package service

import (
	"context"
	"net/rpc"

	"gitlab.com/kuzmem/gateway/internal/infrastructure/errors"
	"gitlab.com/kuzmem/gateway/pkg/processing/auth"
)

type AuthServiceJSONRPC struct {
	client *rpc.Client
}

func NewAuthServiceJSONRPC(client *rpc.Client) *AuthServiceJSONRPC {
	u := &AuthServiceJSONRPC{client: client}

	return u
}

func (a *AuthServiceJSONRPC) Register(ctx context.Context, in auth.RegisterIn) auth.RegisterOut {
	var out auth.RegisterOut
	err := a.client.Call("AuthServiceJSONRPC.Register", in, &out)
	if err != nil {
		out.ErrorCode = errors.AuthServiceRegisterErr
	}

	return out
}

func (a *AuthServiceJSONRPC) AuthorizeEmail(ctx context.Context, in auth.AuthorizeEmailIn) auth.AuthorizeOut {
	var out auth.AuthorizeOut
	err := a.client.Call("AuthServiceJSONRPC.AuthorizeEmail", in, &out)
	if err != nil {
		out.ErrorCode = errors.AuthServiceAuthorizeEmailError
	}

	return out
}

func (a *AuthServiceJSONRPC) AuthorizeRefresh(ctx context.Context, in auth.AuthorizeRefreshIn) auth.AuthorizeOut {
	var out auth.AuthorizeOut
	err := a.client.Call("AuthServiceJSONRPC.AuthorizeRefresh", in, &out)
	if err != nil {
		out.ErrorCode = errors.AuthServiceAuthorizeRefreshError
	}

	return out
}

func (a *AuthServiceJSONRPC) AuthorizePhone(ctx context.Context, in auth.AuthorizePhoneIn) auth.AuthorizeOut {
	var out auth.AuthorizeOut
	err := a.client.Call("AuthServiceJSONRPC.AuthorizePhone", in, &out)
	if err != nil {
		out.ErrorCode = errors.AuthServiceAuthorizePhoneError
	}

	return out
}

func (a *AuthServiceJSONRPC) SendPhoneCode(ctx context.Context, in auth.SendPhoneCodeIn) auth.SendPhoneCodeOut {
	var out auth.SendPhoneCodeOut
	err := a.client.Call("AuthServiceJSONRPC.SendPhoneCode", in, &out)
	if err != nil {
		out.ErrorCode = errors.AuthServiceSendPhoneCodeError
	}

	return out
}

func (a *AuthServiceJSONRPC) VerifyEmail(ctx context.Context, in auth.VerifyEmailIn) auth.VerifyEmailOut {
	var out auth.VerifyEmailOut
	err := a.client.Call("AuthServiceJSONRPC.VerifyEmail", in, &out)
	if err != nil {
		out.ErrorCode = errors.AuthServiceVerifyErr
	}

	return out
}
