package service

import (
	"context"

	"gitlab.com/kuzmem/gateway/internal/infrastructure/errors"
	"gitlab.com/kuzmem/gateway/pkg/processing/auth"
	apb "gitlab.com/kuzmem/gateway/pkg/proto/auth_grpc"
)

type AuthServiceGRPC struct {
	client apb.AutherClient
}

func NewAuthServiceGRPC(client apb.AutherClient) *AuthServiceGRPC {
	return &AuthServiceGRPC{client: client}
}

func (a *AuthServiceGRPC) Register(ctx context.Context, in auth.RegisterIn) auth.RegisterOut {
	resp, err := a.client.Register(ctx, &apb.RegisterRequest{
		Email:          in.Email,
		Phone:          in.Phone,
		Password:       in.Password,
		IdempotencyKey: in.IdempotencyKey,
	})

	if err != nil {
		return auth.RegisterOut{ErrorCode: errors.AuthServiceRegisterErr}
	}

	return auth.RegisterOut{
		Status:    int(resp.GetStatus()),
		ErrorCode: int(resp.GetErrorCode()),
	}
}

func (a *AuthServiceGRPC) AuthorizeEmail(ctx context.Context, in auth.AuthorizeEmailIn) auth.AuthorizeOut {
	resp, err := a.client.AuthorizeEmail(ctx, &apb.AuthorizeEmailRequest{
		Email:          in.Email,
		Password:       in.Password,
		RetypePassword: in.RetypePassword,
	})

	if err != nil {
		return auth.AuthorizeOut{ErrorCode: errors.AuthServiceAuthorizeEmailError}
	}

	return auth.AuthorizeOut{
		UserID:       int(resp.GetUserID()),
		AccessToken:  resp.GetAccessToken(),
		RefreshToken: resp.GetRefreshToken(),
		ErrorCode:    int(resp.GetErrorCode()),
	}
}

func (a *AuthServiceGRPC) AuthorizeRefresh(ctx context.Context, in auth.AuthorizeRefreshIn) auth.AuthorizeOut {
	resp, err := a.client.AuthorizeRefresh(ctx, &apb.AuthorizeRefreshRequest{UserID: int64(in.UserID)})

	if err != nil {
		return auth.AuthorizeOut{ErrorCode: errors.AuthServiceAuthorizeRefreshError}
	}

	return auth.AuthorizeOut{
		UserID:       int(resp.GetUserID()),
		AccessToken:  resp.GetAccessToken(),
		RefreshToken: resp.GetRefreshToken(),
		ErrorCode:    int(resp.GetErrorCode()),
	}
}

func (a *AuthServiceGRPC) AuthorizePhone(ctx context.Context, in auth.AuthorizePhoneIn) auth.AuthorizeOut {
	resp, err := a.client.AuthorizePhone(ctx, &apb.AuthorizePhoneRequest{
		Phone: in.Phone,
		Code:  int64(in.Code),
	})

	if err != nil {
		return auth.AuthorizeOut{ErrorCode: errors.AuthServiceAuthorizePhoneError}
	}

	return auth.AuthorizeOut{
		UserID:       int(resp.GetUserID()),
		AccessToken:  resp.GetAccessToken(),
		RefreshToken: resp.GetRefreshToken(),
		ErrorCode:    int(resp.GetErrorCode()),
	}
}

func (a *AuthServiceGRPC) SendPhoneCode(ctx context.Context, in auth.SendPhoneCodeIn) auth.SendPhoneCodeOut {
	resp, err := a.client.SendPhoneCode(ctx, &apb.SendPhoneCodeRequest{Phone: in.Phone})

	if err != nil {
		return auth.SendPhoneCodeOut{ErrorCode: errors.AuthServiceSendPhoneCodeError}
	}

	return auth.SendPhoneCodeOut{
		Phone:     resp.GetPhone(),
		Code:      int(resp.GetCode()),
		ErrorCode: int(resp.GetErrorCode()),
	}
}

func (a *AuthServiceGRPC) VerifyEmail(ctx context.Context, in auth.VerifyEmailIn) auth.VerifyEmailOut {
	resp, err := a.client.VerifyEmail(ctx, &apb.VerifyEmailRequest{
		Hash:  in.Hash,
		Email: in.Email,
	})

	if err != nil {
		return auth.VerifyEmailOut{ErrorCode: errors.AuthServiceVerifyErr}
	}

	return auth.VerifyEmailOut{
		Success:   resp.GetSuccess(),
		ErrorCode: int(resp.GetErrorCode()),
	}
}
