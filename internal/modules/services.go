package modules

import (
	auservice "gitlab.com/kuzmem/gateway/internal/modules/auth/service"
	eservice "gitlab.com/kuzmem/gateway/internal/modules/exchange/service"
	uservice "gitlab.com/kuzmem/gateway/internal/modules/user/service"
)

type Services struct {
	User     uservice.Userer
	Auth     auservice.Auther
	Exchange eservice.Exchanger
}

func NewServices(userClient uservice.Userer, auther auservice.Auther, exchange eservice.Exchanger) *Services {
	return &Services{
		User:     userClient,
		Auth:     auther,
		Exchange: exchange,
	}
}
