package controller

import (
	"github.com/ptflp/godecoder"
	"gitlab.com/kuzmem/gateway/internal/infrastructure/component"
	"gitlab.com/kuzmem/gateway/internal/infrastructure/errors"
	"gitlab.com/kuzmem/gateway/internal/infrastructure/handlers"
	"gitlab.com/kuzmem/gateway/internal/infrastructure/responder"
	"gitlab.com/kuzmem/gateway/internal/modules/user/service"
	"gitlab.com/kuzmem/gateway/pkg/message/user_mess"
	"gitlab.com/kuzmem/gateway/pkg/processing/user"
	"net/http"
)

type Userer interface {
	Profile(http.ResponseWriter, *http.Request)
	GetUsersInfo(http.ResponseWriter, *http.Request)
}

type User struct {
	service service.Userer
	responder.Responder
	godecoder.Decoder
}

func NewUser(service service.Userer, components *component.Components) Userer {
	return &User{service: service, Responder: components.Responder, Decoder: components.Decoder}
}

func (u *User) Profile(w http.ResponseWriter, r *http.Request) {
	claims, err := handlers.ExtractUser(r)
	if err != nil {
		u.ErrorBadRequest(w, err)
		return
	}
	out := u.service.GetByID(r.Context(), user.GetByIDIn{UserID: claims.ID})
	if out.ErrorCode != errors.NoError {
		u.OutputJSON(w, user_mess.ProfileResponse{
			ErrorCode: out.ErrorCode,
			Data: user_mess.Data{
				Message: "retrieving user_mess error",
			},
		})
		return
	}

	u.OutputJSON(w, user_mess.ProfileResponse{
		Success:   true,
		ErrorCode: out.ErrorCode,
		Data: user_mess.Data{
			User: *out.User,
		},
	})
}

func (u *User) GetUsersInfo(w http.ResponseWriter, r *http.Request) {
	panic("implement me")
}
