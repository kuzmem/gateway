package exchange

import "gitlab.com/kuzmem/gateway/pkg/models"

type CryptoOut struct {
	Currency  []models.CryptoPrice `json:"currency"`
	ErrorCode int                  `json:"error_code"`
}

type CryptoIn struct {
}
