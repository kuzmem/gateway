package models

type CryptoPrice struct {
	ID    int     `json:"id"`
	Pair  string  `json:"pair"`
	Price float64 `json:"price"`
}
