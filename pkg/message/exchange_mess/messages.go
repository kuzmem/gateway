package exchange_mess

import "gitlab.com/kuzmem/gateway/pkg/models"

//go:generate easytags $GOFILE

type Data struct {
	Message string
	List    []models.CryptoPrice
}

type ListResponse struct {
	Success   bool
	ErrorCode int
	Data      Data
}
